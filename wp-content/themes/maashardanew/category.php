<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
		
	<section id="primary" class="site-content">
		<div id="content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php// printf( __( 'Category Archives: %s', 'twentytwelve' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>

			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
			<?php endif; ?>
			</header><!-- .archive-header -->

			<?php
			/* Start the Loop 
			while ( have_posts() ) : the_post();

				/* Include the post format-specific template for the content. If you want to
				 * this in a child theme then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				
				get_template_part( 'content', get_post_format() );

			endwhile;

			twentytwelve_content_nav( 'nav-below' );*/
			?>
			<div class="info_block gray_bg padding15" id="knowus" data-testing="Know us">
			<h3><?php printf( __( '%s', 'twentytwelve' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?><span class="under_line white_bg"></span>
			</h3>
			<div class="row">
				<?php while ( have_posts() ) : the_post();?>
				<div class="col-xs-12 col-md-4">
					<div class="knowus_block">
						<h4><?php the_title();?></h4>
							<!---<div class="image_box"><?php //the_post_thumbnail();?></div>image display--->
							<div class="text_box">
								<p><?php the_excerpt();?></p>
							</div>
							<a href="<?php the_permalink();?>" class="view-more"> <span class="line"></span> <span class="fa fa-angle-right"></span> </a> 
					</div>
				</div>
				<?php endwhile;?>
			</div>
        </div>
		<div style="clear: both;"></div>
		<div class="info_block"></div>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
