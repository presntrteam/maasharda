<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); 
$home_id = 5;
?>
		<div class="topgallery">
			<div id="gallery"> <a href="#">
				<img alt="What makes us diffrent" 
			 src="<?php echo get_template_directory_uri(); ?>/images/galler1.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler1.jpg"
			 data-description="What makes us diffrent"
			 style="display:none"> </a> <a href="#">
				 <img alt="Know us"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler2.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler2.jpg"
			 data-description="Know us"
			 style="display:none"> </a> <a href="#">
				 <img alt="The things which we do well"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler3.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler3.jpg"
			 data-description="The things which we do well"
			 style="display:none"> </a> <a href="#">
				 <img alt="See our clinic"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler5.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler5.jpg"
			 data-description="See our clinic"
			 style="display:none"> </a> <a href="#">
				 <img alt="Let's get in touch"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler6.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler5.jpg"
			 data-description="Let's get in touch"
			 style="display:none"> </a> <a href="#">
				 <img alt="Be the first to read"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler4.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler6.jpg"
			 data-description="Be the first to read"
			 style="display:none"> </a> <a href="#">
				 <img alt="Succsess story"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler7.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler7.jpg"
			 data-description="Succsess story"
			 style="display:none"> </a> <a href="#">
				 <img alt="Our Team"
			 src="<?php echo get_template_directory_uri(); ?>/images/galler8.jpg"
			 data-image="<?php echo get_template_directory_uri(); ?>/images/galler8.jpg"
			 data-description="Our Team"
			 style="display:none"> </a>
			</div>
		</div>
        <div class="timing_block">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 time_block_listing">
					<div class="time_block_data">
						<h4><span class="data_icon"><i class="fa fa-clock-o"></i></span><?php echo get_post_meta ( $home_id, 'working_hours_title', true ); ?></h4>
							<?php echo get_post_meta ( $home_id, 'working_hours', true ); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 time_block_listing">
					<div class="time_block_data">
						<h4><span class="data_icon"><i class="fa fa-hourglass-half"></i></span> <?php echo get_post_meta ( $home_id, 'opening_hours_title', true ); ?></h4>
					 <?php echo get_post_meta ( $home_id, 'opening_hours', true ); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 time_block_listing">
					<div class="time_block_data">
						<h4><span class="data_icon"><i class="fa fa-pencil-square-o"></i></span><?php echo get_post_meta ( $home_id, 'appointment_title', true ); ?></h4>
							<?php echo get_post_meta ( $home_id, 'appointment', true ); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 time_block_listing">
					<div class="time_block_data">
						<h4><span class="data_icon"><i class="fa fa-ambulance"></i></span><?php echo get_post_meta ( $home_id, 'emergency_cases_title', true ); ?></h4>
							<?php echo get_post_meta ( $home_id, 'emergency_cases', true ); ?>
					</div>
				</div>
			</div>
        </div>
        <div class="info_block has_image" id="What-diffrent" data-testing="What makes us diffrent">
				<?php
					$post = get_post('7'); //assuming $id has been initialized
					setup_postdata($post);
				?>
				<h3 class="entry-title"><span><?php the_title(); ?></span><span class="under_line gray_bg"></span></h3>
					<div class="text_box">
						<?php
							$thumbs = wp_get_attachment_image_src( get_post_thumbnail_id($post));
						?> 
						<img src="<?php echo $thumbs[0]; ?>" style="height: 280px;">
						<?php
							the_content('',TRUE);
						?>
						<a href="<?php echo the_permalink();?>" class="view-more"> <span class="line"></span> <span class="fa fa-angle-right"></span> </a>
						<?php wp_reset_postdata();?>
					</div>
					<div class="clearfix"></div>
        </div>
        <div class="info_block gray_bg padding15" id="knowus" data-testing="Know us">
			<h3><span>
					<?php 
						$cat_data = get_category_by_slug('know-us'); 
						echo $cat_data->name;
					?> 
				</span><span class="under_line white_bg"></span>
			</h3>
			<div class="row">
			   <?php
					global $post;
					$args = array('numberposts' => 3,'category_name' => 'know-us' );
					$posts = get_posts( $args );
					foreach( $posts as $post ): setup_postdata($post); 
					//echo "hi";exit;
				?>
				<div class="col-xs-12 col-md-4">
					<div class="knowus_block">
						<h4><?php the_title();?></h4>
							<div class="image_box"><?php the_post_thumbnail();?> </div>
							<div class="text_box">
								<p><?php the_content('',TRUE);?></p>
							</div>
							<a href="<?php the_permalink();?>" class="view-more"> <span class="line"></span> <span class="fa fa-angle-right"></span> </a> 
					</div>
				</div>
				<?php
					wp_reset_postdata();
				?>
				<?php
					endforeach;
				?>
			</div>
        </div>
		 <div class="info_block" id="ourTeam" data-testing="Our Team">
			<h3><span>our TEAM</span><span class="under_line gray_bg"></span></h3>
				<?php
					$args = array( 'post_type' => 'our_team');
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
					$team_id = get_the_ID();
				?>
				<ul class="team_listing">
					<li> <a href="#" class="team_photo" data-team_id="<?php echo $team_id; ?>" data-toggle="modal" data-target="#myModal"> 
						<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($team_id), 'our_team');
						?> 
							<img src="<?php echo $thumb[0]; ?>" style="height: 239px;">
							<div class="hover_block">
								<div class="zoom"> <span></span> <i class="fa fa-search"></i> </div>
							</div>
						</a>
						
						<div class="team_info">
							
							<p><?php echo get_post_meta ( $team_id, 'designation', true ); ?></p>
							<h4><?php the_title();?></h4>
							<ul class="social_info">
							  <li><a href="<?php echo get_post_meta ( $team_id, 'facebook_link', true ); ?>"><span class="fa fa-facebook"></span></a></li>
							  <li><a href="<?php echo get_post_meta ( $team_id, 'twitter_link', true ); ?>"><span class="fa fa-twitter"></span></a></li>
							  <li><a href="<?php echo get_post_meta ( $team_id, 'google_plus_link', true ); ?>"><span class="fa fa-google-plus"></span></a></li>
							  <li><a href="<?php echo get_post_meta ( $team_id, 'linked_in_link', true ); ?>"><span class="fa fa-linkedin"></span></a></li>
							</ul>
						</div>
					</li>
				</ul>
				
			<?php
				endwhile;
			?>
        </div>
		<div style="clear: both;"></div>
		<div class="info_block" data-testing="The things which we do well" id="thingsWeDo" >
			<?php
				$post = get_post('14');
				setup_postdata($post);
			?>
			<h3><span><?php the_title(); ?></span><span class="under_line gray_bg"></span></h3>
			<div class="text_box">
				<p><?php the_content('',TRUE);?></p>
				<a href="<?php the_permalink();?>" class="view-more"> <span class="line"></span> <span class="fa fa-angle-right"></span> </a>
			</div>
			<?php
				wp_reset_postdata();
			?>
        </div>
		<div class="info_block fitter" id="clinic" data-testing="See our clinic">
			<h3><span><?php echo get_post_meta ( $home_id, 'portfolio_name', true ); ?></span><span class="under_line gray_bg"></span></h3>
			<div style="clear: both;"><div>
				<?php echo do_shortcode( '[fgp_portfolio num_columns=3]' ); ?>
			</div>
        </div>
        <div style="clear: both;"></div>
		  <div class="info_block gray_bg" id="befirst" data-testing="Be the first to read">
			<?php 
				$cat_data = get_category_by_slug('be-the-first-to-read-it'); 				
			?>
			<h3><span><?php echo $cat_data->name;?></span><span class="under_line white_bg"></span></h3>
			<?php
				global $post;
				$args = array('numberposts' => 3,'category_name' => 'be-the-first-to-read-it' );
				$posts = get_posts( $args );
				foreach( $posts as $post ): setup_postdata($post); 
				//echo "hi";exit;
				$i++;
			?>
					<ul class="first-readit">
						<?php if(($i % 2) !== 0) : ?>
							<li> 
								<a href="<?php the_permalink();?>"> <span class="info fl fontright"><?php the_content();?> </span> <span class="name fr"> <span class="articalname"><?php the_title();?></span><br>
								<span class="date"><?php the_date(); ?></span><br>
								<span class="eye"><i class="fa fa-eye"></i></span> </span> <span class="clearfix"></span> 
								</a> 
							</li>
						<?php  else :?>
							<li> 
								<a href="<?php the_permalink();?>"> <span class="name fl"> <span class="articalnameleft"><?php the_title();?></span><br>
								<span class="dateleft text-right"><?php the_date(); ?></span><br>
								<span class="eyeleft text-right"><i class="fa fa-eye"></i></span> </span> <span class="info fr fontleft"> <?php the_excerpt();?></span> <span class="clearfix"></span>
								</a>
							</li>
						<?php endif;?>
						<?php
							wp_reset_postdata();
						?>
				  </ul>
			<?php
				endforeach;
				$cat = get_category_by_slug('be-the-first-to-read-it');
				$link = get_category_link($cat);
				//print_r($link);
			?>
			<button class="btn whitebtn center-block"><a href="<?php echo esc_url( $link ); ?>">View All</a></button>
        </div>
        <div style="clear: both;"></div>
        <div class="info_block gray_bg padding15" id="image_video" data-testing="image & Video">
        	<h3><span>
					<?php
						$obj=get_post_type_object('image_videos');
						echo $obj->labels->singular_name;
					?>	
				</span><span class="under_line white_bg"></span>
			</h3>
        	<div class="row">
			<?php	
				$custom_terms = get_terms('image_video_type');
				foreach($custom_terms as $custom_term) {
				    wp_reset_query();
				    $args = array('posts_per_page' => 1,'post_type' => 'image_videos',
				        'tax_query' => array(
				            array(
				                'taxonomy' => 'image_video_type',
				                'field' => 'slug',
				                'terms' => $custom_term->slug,
				            ),
				        ),
				     );

				     $loop = new WP_Query($args);
				     if($loop->have_posts()) {
				?>
						
					   <div class="col-xs-12 col-md-4">
							<div class="knowus_block">
								<h4><?php echo $custom_term->name; ?></h4>
								<?php while($loop->have_posts()) : $loop->the_post(); ?>
									<?php if($custom_term->slug == 'operative-video'){ ?>	
										<div class="text_box">
											<p><?php the_content();?></p>
										</div>
									<?php } else { ?>
									
										<div class="image_box">
											<?php
												$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($custom_term->ID));
											?> 
												<a href="<?php echo get_term_link( $custom_term ); ?>"><img src="<?php echo $thumb[0]; ?>" style="height: 280px;"></a>
										</div>
										<div class="text_box">
											<p><?php the_excerpt();?></p>
										</div>
									<?php } ?>
									<a href="<?php echo get_term_link( $custom_term ); ?>" class="view-more"> <span class="line"></span> <span class="fa fa-angle-right"></span> </a>
								<?php endwhile; ?>
							</div>
						</div>
				<?php
				     }
				}
			?>
					
			</div>
		</div>
      
        
        <div class="info_block" id="story" data-testing="Succsess story">
		  <h3><span><?php echo get_post_meta ( $home_id, 'testimonial_title', true ); ?> </span><span class="under_line gray_bg"></span></h3>
            <div class="story_slide">
			   <?php
					$args = array( 'post_type' => 'success_story');
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
				?>
				<div>
				  <div class="slide_listing">
					<p><?php the_content();?></p>
					<h4><?php the_title();?> <small><?php echo get_post_meta ( $team_id, 'designation', true ); ?></small></h4>
				  </div>
				</div>
				 <?php
					endwhile;
				?>
			</div>
				
        </div>
		<div class="footerbefore" id="appointment" data-testing="Let">
			<div class="left">
				<?php echo do_shortcode('[contact-form-7 id="118" title="contact us"]');?>
			</div>
			<div class="right">
				<div class="my_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1482128389110" width="100%" height="600px" frameborder="0" style="border:0; -webkit-filter: grayscale(100%); filter: grayscale(100%);" allowfullscreen></iframe>
				</div>
			</div>
		  <div class="clearfix"></div>
		</div>

<?php get_footer(); ?>
