<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
 $home_id = 5;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
	<![endif]-->
	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/slick.css" rel="stylesheet">
	<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/unite-gallery.css' type='text/css' />
	<link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page">
		<div class="sticky_info">
			<div class="container-fluid">
				<ul>
					 <li><i class="top_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/teliphone.png"></i><span><?php echo get_post_meta($home_id,'telephone',true);?></span></li>
					 <li><i class="top_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/call.png"></i><span><?php echo get_post_meta($home_id,'mobile',true);?></span></li>
					 <li><i class="top_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/appoinment.png"></i><span class="appointment" style="cursor: pointer;"><?php echo get_post_meta($home_id,'appointment_text',true);?></span></li>
					 <li><i class="top_icon"><img src="<?php echo get_template_directory_uri(); ?>/images/mail.png"></i><span><?php echo get_post_meta($home_id,'mail',true);?></span></li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-3 col-lg-2 left_panel">
					<div class="logo_block" > <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo-verticle.png"> <div class="tooltip"><?php echo get_post_meta ( $home_id, 'tooltip', true ); ?></div> </a> </div></div>
				</div>
				<div class="col-xs-12 col-md-9 col-lg-10 col-xs-offset-0 col-md-offset-3 col-lg-offset-2 right_panel">
       
         
