<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<link href="<?php echo get_template_directory_uri(); ?>/bootstrap-vertical-tab/bootstrap-vertical-tabs.css" rel="stylesheet">

	

	<section class="">
		<div class="">

		<?php
			$current_post_type = '';
			print_r($current_slug);
			$current_post_type = get_post_type();
		?>
		
		<?php if ( have_posts() ) : ?>

			<?php if( $current_post_type == 'image_videos'){ ?>
				<div class="info_block has_image">
					<h3 class="entry-title"><span><?php single_cat_title(); ?></span><span class="under_line gray_bg"></span></h3>
					<div class="row">
						<div class="col-xs-3"> <!-- required for floating -->
							<!-- Nav tabs -->
							<ul class="nav nav-tabs tabs-left">
								<?php
									$tab_title = 1 ;
									while ( have_posts() ) : the_post();
								?>
									<li class="<?php if($tab_title == 1 ){ echo 'active'; } ?> "><a href="#post_<?php echo $post->ID; ?>" data-toggle="tab"><?php the_title(); ?></a></li>
								<?php
									$tab_title++;
									endwhile;
								?>
							</ul>
						</div>

						<div class="col-xs-9">
							<!-- Tab panes -->
							<div class="tab-content">
								<?php
									$tab_desc = 1 ;
									while ( have_posts() ) : the_post();
								?>
									<div class="tab-pane <?php if($tab_desc == 1 ){ echo 'active'; } ?>" id="post_<?php echo $post->ID; ?>">
										<?php //the_title(); ?>
										<?php the_post_thumbnail(); ?>
										<?php the_content(); ?>
									</div>
								<?php
									$tab_desc++;
									endwhile;
								?>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

			<?php } else { ?>
		
				<header class="archive-header">
					<h1 class="archive-title"><?php
						if ( is_day() ) :
							printf( __( 'Daily Archives: %s', 'twentytwelve' ), '<span>' . get_the_date() . '</span>' );
						elseif ( is_month() ) :
							printf( __( 'Monthly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentytwelve' ) ) . '</span>' );
						elseif ( is_year() ) :
							printf( __( 'Yearly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentytwelve' ) ) . '</span>' );
						else :
							_e( 'Archives', 'twentytwelve' );
						endif;
					?></h1>
				</header><!-- .archive-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/* Include the post format-specific template for the content. If you want to
					 * this in a child theme then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

				endwhile;

				twentytwelve_content_nav( 'nav-below' );
				?>
			<?php } ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_footer(); ?>
