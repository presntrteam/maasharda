<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<?php
get_header(); 
?>
<div class="info_block">
		<?php while ( have_posts() ) : the_post(); ?>
				
			
          <h3><span><?php the_title();?></span><span class="under_line gray_bg"></span></h3>
          <div class="text_box">
			<div align="center"><?php the_post_thumbnail();?></div><br>
            <p><?php the_content();?></p>
            </div>
			<?php endwhile; // end of the loop. ?>
        </div>
<?php get_footer(); ?>
