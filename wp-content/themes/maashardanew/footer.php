<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
 $home_id = 5;
?>
			<div class="footer_block">
				<div class="foot_info">
					<div class="container"> 
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4 foot_listing">
								<h4>Contact Information</h4>
								<span class="border-line"></span>
									<div class="contact_info">
										<?php
											if(is_active_sidebar('footer-sidebar-1')){
												dynamic_sidebar('footer-sidebar-1');
											}		
										?>
									</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 foot_listing">
								<h4>Site links</h4>
								<span class="border-line"></span>
								<ul class="foot_links">
								   <?php
										if(is_active_sidebar('footer-sidebar-2')){
											dynamic_sidebar('footer-sidebar-2');
										}		
									?>
								</ul>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 foot_listing">
								<h4>Site links</h4>
								<span class="border-line"></span>
								<ul class="foot_links">
									<?php
										if(is_active_sidebar('footer-sidebar-3')){
											dynamic_sidebar('footer-sidebar-3');
										}		
									?>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="copyright"><?php echo get_post_meta ( $home_id, 'copyright_footer', true ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="marquee_container">
					<div class="container-fluid">
						<div class="marquee col-xs-12 col-md-9 col-lg-10 col-xs-offset-0 col-md-offset-3 col-lg-offset-2">
							<marquee onmouseover="this.stop();" onmouseout="this.start();">
								<?php echo get_post_meta ( $home_id, 'footer_marquee', true ); ?>
							</marquee>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
  </div>
</div>

<!-- popup Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Team Information</h4>
			</div>
			<div class="modal-body team_details"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/scrollreveal.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/fitter.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.js"></script> 
<script type="text/javascript">
    	$(document).ready(function(){



//$(".logo").click(function() {
setTimeout(function(){
	
$(".ug-textpanel-bg, .ug-textpanel-title").click(function() {
		var id_name = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
		//alert(id_name);
		if ($('#knowus').attr('data-testing') ==  id_name){
			
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#knowus').offset().top -= 80},
		'slow');
		}
		else if ($('#What-diffrent').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#What-diffrent').offset().top -= 80},
		'slow');
		}
		else if ($('#ourTeam').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#ourTeam').offset().top -= 80},
		'slow');
		}
		else if ($('#story').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#story').offset().top -= 80},
		'slow');
		} else if ($('#appointment').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#appointment').offset().top -= 80},
		'slow');
		} else if ($('#thingsWeDo').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#thingsWeDo').offset().top -= 80},
		'slow');
		}  else if ($('#clinic').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#clinic').offset().top -= 80},
		'slow');
		}  else if ($('#befirst').attr('data-testing') ==  id_name){
			var idscroll = $(this).parents(".ug-textpanel").siblings(".ug-thumb-image").attr("alt");
			$('html,body').animate({
		scrollTop: $('#befirst').offset().top -= 80},
		'slow');
		} else {
			//alert("else");
			// do that
		}
		
		
});
}, 1000);


$('.scroll_to_top').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 600);
	return false;
});
						
			var page_height = $(".footer_block").height();
			//$("#page").css("margin-bottom", page_height);
			$('.story_slide').slick({
				  dots: true,
				  infinite: true,
				  speed: 300,
				  slidesToShow: 1,
				  adaptiveHeight: true
				  
			});
		
			// Select and loop the container element of the elements you want to equalise
			$('.timing_block').each(function(){  
			  
			  // Cache the highest
			  var highestBox = 0;
			  
			  // Select and loop the elements you want to equalise
			  $('.time_block_data', this).each(function(){
				
				// If this box is higher than the cached highest then store it
				if($(this).height() > highestBox) {
				  highestBox = $(this).height(); 
				}
			  
			  });  
					
			  // Set the height of all those children to whichever was highest 
			  $('.time_block_data',this).height(highestBox);
							
			}); 
						
			/*on scrool eefect add time to blocks*/
			window.sr = ScrollReveal();
			sr.reveal('.time_block_data', { duration: 1000}, 500);
			sr.reveal('.info_block:nth-child(2) .text_box', { duration: 1000 }, 1000);
			sr.reveal('.info_block:nth-child(6) .text_box', { duration: 1000 }, 1000);
			sr.reveal('.knowus_block', { duration: 1000 }, 1000);
			sr.reveal('.team_listing li', { duration: 500 }, 100);
			sr.reveal('.slick-slider', { duration: 1000 }, 1000);


		
			
			
	
		});		
    </script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/unitegallery.min.js'></script> 
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/ug-theme-tiles.js'></script> 
<script type="text/javascript">
	jQuery(window).load(function(){
		jQuery('.ug-gallery-wrapper.ug-lightbox').remove();	
	});
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	jQuery(document).ready(function(){
		jQuery('.team_photo').click(function(){
			var team_id = jQuery(this).data('team_id');
			jQuery.ajax({
				url: ajaxurl,
				data : {action: "get_team_details", team_id: team_id},
				success: function(result){
					jQuery('.team_details').html('');
					jQuery('.team_details').html(result);
				},
			});
		});
		jQuery("#gallery").unitegallery({
			//tile_border_color:"#7a7a7a",
			//tile_outline_color:"#8B8B8B",
			//tile_enable_shadow:true,
			//tile_shadow_color:"#8B8B8B",
			tile_overlay_opacity:0.3,
			tile_show_link_icon:false,
			tile_image_effect_type:"sepia",
			tile_image_effect_reverse:true,
			tile_enable_textpanel:true,
			lightbox_textpanel_title_color:"e5e5e5",
			tiles_col_width:320,
			tiles_space_between_cols:5				
		});
	});
/*Added by Nikunj Onclick Scroll*/
	jQuery(".appointment").click(function() {
 		$('html,body').animate({
		scrollTop: $("#appointment").offset().top -= 80},
		'slow');
		jQuery("input[name=Name]").focus();
	});
	jQuery('a[href="#What-diffrent"]').click(function() {
 		$('html,body').animate({
		scrollTop: $('#What-diffrent').offset().top -= 80},
		600);
	});
	jQuery('a[href="#knowus"]').click(function() {
 		$('html,body').animate({
		scrollTop: $('#knowus').offset().top -= 80},
		'slow');
	});
	jQuery('a[href="#thingsWeDo"]').click(function() {
 		$('html,body').animate({
		scrollTop: $('#thingsWeDo').offset().top -= 80},
		'slow');
	});
	jQuery('a[href="#clinic"]').click(function() {
 		$('html,body').animate({
		scrollTop: $('#clinic').offset().top -= 80},
		'slow');
	});
	jQuery('a[href="#appointment"]').click(function() {
 		$('html,body').animate({	
		scrollTop: $('#appointment').offset().top -= 80},
		'slow');
	});
	jQuery('a[href="#befirst"]').click(function() {
 		$('html,body').animate({
		scrollTop: $('#befirst').offset().top -= 80},
		'slow');
	});
	jQuery('a[href="#story"]').click(function() {
		$('html,body').animate({
		scrollTop: $('#story').offset().top -= 80},
		'slow');
	});
	
</script>
<?php wp_footer(); ?>
<a class="scroll_to_top" href="#">
	<i class="glyphicon glyphicon-triangle-top"></i>
</a> 
</body>
</html>
